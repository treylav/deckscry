"""
Using magicthegathering.io API looks for the last reprint of the specified card.
Returns it as a Card-type object.
"""

from mtgsdk import Card


def strict_request(name: str, language: str) -> Card:
    # looking for only a specific card, so the query in quotes
    request = Card.where(language=language).where(name=('"{}"'.format(name))).all()
    # looking for the most recent reprint
    last_print = Card()
    last_print.multiverse_id = 0
    for item in request:
        if not item.multiverse_id:  # ignore cards without id
            continue
        if item.multiverse_id > last_print.multiverse_id:
            last_print = item
    return last_print if last_print.name else None
