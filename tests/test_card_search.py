from deckscry import card_search


def strict_request_english_test():
    sample = card_search.strict_request('go for the throat', '')
    assert sample.name == 'Go for the Throat'
    assert sample.set_name == 'Commander 2017'


def strict_request_russian_test():
    sample = card_search.strict_request('червячник из пищащей ватрушки', 'Russian')
    assert sample.name == 'Squeaking Pie Grubfellows'
    assert sample.set_name == 'Morningtide'
